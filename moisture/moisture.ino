/*
  Web client

  This sketch connects to a website (www.roberts.tk/api)
  using the WiFi module.

  This example is written for a network using WPA encryption. For
  WEP or WPA, change the Wifi.begin() call accordingly.

  Circuit:
   Board with NINA module (Arduino MKR WiFi 1010)

  created 10 mar 2019
  by dlf (Metodo2 srl)
  modified 31 Mar 2019
  by Roberts Kemps
*/
const int moistureSensor = 1;
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(moistureSensor);
  // print out the value you read:
  Serial.println(sensorValue);
  delay(500);        // delay in between reads for stability
}
