/*
  Web client

  This sketch connects to a website (www.roberts.tk/api)
  using the WiFi module.

  This example is written for a network using WPA encryption. For
  WEP or WPA, change the Wifi.begin() call accordingly.

  Circuit:
   Board with NINA module (Arduino MKR WiFi 1010)

  created 10 Mar 2019
  modified 31 Mar 2019
  by Roberts Kemps
*/
#include <WiFiNINA.h>
//Define wifi SSID and Password
char ssid[] = "Tele2_0E434B_2.4G";        
char pass[] = "Pavasara29";

//Define variables  
String postResponse;
String sensorMin;
String sensorMax;
int avarageMoisture;
int status = WL_IDLE_STATUS;
char server[] = "www.roberts.tk";
int sensorValue;
int moisturePrecentage;
bool relayStatus;

//Define arduino pins
const int moistureSensor = 1;
const int relayModule = 1;


// Initialize ethernet client library (with default port :80)
WiFiClient client;

void setup() {
  //Initialize serial and relay pin
  Serial.begin(9600);
  pinMode(relayModule, OUTPUT);

  //Call function that connects to wifi
  connectToWifi();
}

void loop() {
  Serial.println("Loop starts");
  digitalWrite(relayModule, LOW);
  relayStatus = false;
  //if the server's disconnected, stop the client:
  if (!client.connected()) {
    sendGET();

    //Loop for 5 minutes and check if moisture is below or above the min/max values
    uint32_t period = 5 * 60000L; // 5 min
    for(uint32_t tStart = millis(); (millis()-tStart) < period;){
      //atoi(sensorMin.c_str() converts string into int
      sensorValue = analogRead(moistureSensor);
      moisturePrecentage = (sensorValue*100)/700;
      int minPercentage = atoi(sensorMin.c_str());
      if(moisturePrecentage <= avarageMoisture){
          digitalWrite(relayModule, HIGH);
          relayStatus = true;
      }
      if(moisturePrecentage >= avarageMoisture){
          digitalWrite(relayModule, LOW);
          relayStatus = false;
      }
      delay(300);
    }    
  }else{
    connectToWifi();
  }
}

void connectToWifi(){
  //Check if wifi module is working
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Wifi module not found");
    while (true);
  }

  // Try to connect to Wifi network:
  while (status != WL_CONNECTED) {
    
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    
    status = WiFi.begin(ssid, pass);
    //Wait 4 sec to connect
    delay(4000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();
}

void sendGET() //client function to send/receive GET request data.
{
  //Turn off the relay while request is beeing sent
  digitalWrite(relayModule, LOW);
  //Makes GET request to defined servers port 80
  if (client.connect(server, 80)) {  
    Serial.println("connected");
    //Send the request header
    client.println("GET /api?sensor="+String(sensorValue)+" HTTP/1.1");
    client.println("Host: www.roberts.tk");
    client.println("Connection: close");
    client.println();
  } 
  else {
    Serial.println("connection failed"); //error message if no client connect
    Serial.println();
  }

  while(client.connected() && !client.available()) delay(1); //Waits for the response from the server
  //Reads the response in the postResponse variable
  while (client.connected() || client.available()) {
     postResponse = client.readString(); 
  }

  //Define variables that will be needed to save the response data
  bool readVariable;
  int varCounter = 0;
  bool skippedFirst = false;
  bool skippedSecond = false;
  sensorMin = "";
  sensorMax = "";
  for (int i = 0; i <= postResponse.length(); i++) {
    if (postResponse[i] == '^') {
      varCounter++;
    }
    if (varCounter == 1) {
      if (skippedFirst) {
        sensorMin += postResponse[i];
      }
      skippedFirst = true;
    }
    if (varCounter == 2) {
      if (skippedSecond) {
        sensorMax += postResponse[i];
      }
      skippedSecond = true;
    }
  }
  atoi(sensorMin.c_str())
  avarageMoisture = (atoi(sensorMin.c_str()) + atoi(sensorMax.c_str())/2);
  client.stop(); //stop client
}

//-----------Nav obligāts, pēc izstrādes aizkomentēt/dzēst------------------
void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
